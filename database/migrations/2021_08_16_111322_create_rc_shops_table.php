<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_shops', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->bigInteger('shopify_store_id')->nullable();
            $table->boolean('test_store')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('myshopify_domain')->nullable();
            $table->string('domain')->nullable();
            $table->string('owner')->nullable();
            $table->string('shopify_plan')->nullable()->comment('plan_name from API');
            $table->string( 'timezone')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('checkout_api_supported')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('country_code')->nullable();
            $table->string('country_name')->nullable();
            $table->string('country_taxes')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('currency')->nullable();
            $table->string('currency_symbol', 10)->nullable();
            $table->string('zip')->nullable();
            $table->string('primary_locale')->nullable();
            $table->string('province')->nullable();
            $table->string('province_code')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_shops');
    }
}
