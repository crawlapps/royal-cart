<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcAbandonedCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_abandoned_checkouts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');
            $table->string('sh_checkout_id')->nullable();
            $table->string('sh_customer_id')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('source_name')->nullable();
            $table->longText('abandoned_checkout_url')->nullable();
            $table->dateTime('checkout_created_at')->nullable();
            $table->boolean('is_ordered')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_abandoned_checkouts');
    }
}
