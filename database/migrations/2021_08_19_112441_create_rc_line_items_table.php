<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_line_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('db_checkout_id');

            $table->string('title')->nullable();
            $table->string('quantity')->nullable();
            $table->decimal('price', 8,2)->nullable();
            $table->decimal('total_price', 8,2)->nullable();
            $table->string('image')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('db_checkout_id')->on('rc_abandoned_checkouts')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_line_items');
    }
}
