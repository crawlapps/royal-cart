<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubscribersColumnToRcCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_customers', function (Blueprint $table) {
            $table->boolean('is_emailsubscriber')->after('total_spend_currency')->default(1);
            $table->boolean('is_smssubscriber')->after('is_emailsubscriber')->default(1);
            $table->boolean('is_pushsubscriber')->after('is_smssubscriber')->default(1);

            $table->dropColumn('is_subscriber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_customers', function (Blueprint $table) {
            //
        });
    }
}
