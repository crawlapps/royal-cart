<?php
return [
    'default' => [
        'email' => [
            [
                'discount_type' => 0,
                'discount_value' => 0,
                'sending_time' => 30,
                'sending_type' => 0,
                'is_default' => 1,
                'body' => [
                    'body_text' => '',
                    'unsubscribe_text' => '',
                    'subject' => 'Your order status...',
                    'email_preview' => '[ACTION REQUIRED] your order is not complete',
                    'header' => 'Your order was not processed',
                    'before_cart_body' => 'Hi {firstName},
Thanks for choosing {siteName}! You\'ve placed items in your shopping cart, but didn’t complete checkout.
We saved your cart for you, so none of your items run out',
                    'after_cart_body' => '',
                    'discount_banner_text' => 'Your shopping cart has been reserved for 20 minutes!',
                    'footer' => 'You received this email because you’ve started a checkout at {siteName} and didn’t finish. If you have any questions, please feel free to reach out to us at:


{storeEmail}
{unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                    'cart_title' => 'Your cart:',
                    'product_description' => 'Product',
                    'product_price' => 'Price',
                    'product_qty' => 'Quantity',
                    'product_total' => 'Total',
                    'cart_total' => 'Total:',
                    'button_text' => 'Complete My Order',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 15,
                'sending_time' => 24,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '',
                    'unsubscribe_text' => '',
                    'subject' => '{firstName}, are you still thinking about it?',
                    'email_preview' => 'Get {discountValue}% OFF on the items your left in your cart',
                    'header' => 'Are you still thinking about it?',
                    'before_cart_body' => 'Hi {firstName},
We know there are many reasons you might still be thinking about completing your purchase, but we think these items are perfect for you.
So to help you make a decision, here\'s a {discountValue}% discount on your cart, from us to you.',
                    'after_cart_body' => 'Was there another reason you didn\'t complete the purchase? Replay to this email and let us know so we can help!
Otherwise, {cartLink}click here{/cartLink} to complete your purchase.
Thanks for shopping,
           The {siteName} Team',
                    'discount_banner_text' => 'Your cart is now available for {discountValue}% OFF!',
                    'footer' => 'You received this email because you’ve started a checkout at {siteName} and didn’t finish. If you have any questions, please feel free to reach out to us at:

{storeEmail}
{unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                    'cart_title' => 'Your cart:',
                    'product_description' => 'Product',
                    'product_price' => 'Price',
                    'product_qty' => 'Quantity',
                    'product_total' => 'Total',
                    'cart_total' => 'Total:',
                    'button_text' => 'Claim My {discountValue}% Discount',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 15,
                'sending_time' => 72,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '',
                    'unsubscribe_text' => '',
                    'subject' => '{Reminder} your {discountValue}% discount is still waiting',
                    'email_preview' => '{firstName}, claim your {discountValue}% discount before it\'s too late',
                    'header' => 'Just a friendly reminder- your discount is still waiting',
                    'before_cart_body' => '{firstName},

Don\'t miss out on this special discount! Your cart is still reserved, and your discount still waiting for you..',
                    'after_cart_body' => 'Don\'t wait for your items to sell out,  get them now: {cartLink}click here{/cartLink} to complete your purchase.
Thanks for shopping,
           The {siteName} Team',
                    'discount_banner_text' => 'Claim your cart for {discountValue}% OFF!',
                    'footer' => 'You received this email because you’ve started a checkout at {siteName} and didn’t finish. If you have any questions, please feel free to reach out to us at:

{storeEmail}
{unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                    'cart_title' => 'Your cart:',
                    'product_description' => 'Product',
                    'product_price' => 'Price',
                    'product_qty' => 'Quantity',
                    'product_total' => 'Total',
                    'cart_total' => 'Total:',
                    'button_text' => 'Claim My {discountValue}% Discount',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 30,
                'sending_time' => 96,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '',
                    'unsubscribe_text' => '',
                    'subject' => 'Complete your purchase with {discountValue}% OFF',
                    'email_preview' => '{firstName}, your deal is waiting inside',
                    'header' => '{firstName}, your cart is still waiting for you, with a {discountValue}% discount!',
                    'before_cart_body' => '{We are not ready for you to give up on the items you want!


They are still reserved, just for you, and we added a discount to help you get rid of any second thoughts.',
                    'after_cart_body' => 'Don\'t wait for your items to sell out,  get them now: {cartLink}click here{/cartLink} to complete your purchase.
Thanks for shopping,
           The {siteName} Team',
                    'discount_banner_text' => 'Complete your purchase with {discountValue}% OFF!',
                    'footer' => 'You received this email because you’ve started a checkout at {siteName} and didn’t finish. If you have any questions, please feel free to reach out to us at:

{storeEmail}
{unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                    'cart_title' => 'Your cart:',
                    'product_description' => 'Product',
                    'product_price' => 'Price',
                    'product_qty' => 'Quantity',
                    'product_total' => 'Total',
                    'cart_total' => 'Total:',
                    'button_text' => 'Give Me My {discountValue}% Discount',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 30,
                'sending_time' => 48,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '',
                    'unsubscribe_text' => '',
                    'subject' => '{LAST CHANCE} Get your items for {discountValue}% OFF',
                    'email_preview' => 'Your cart is about to expire',
                    'header' => 'Last chance to grab your items from your cart',
                    'before_cart_body' => 'Hi {firstName},

We can only reserve your cart for 24 more hours, after that, we can\'t guarantee your items remain in stock.
Don\'t miss out- get the items you loved now for an amazing discount:',

                    'after_cart_body' => '',
                    'discount_banner_text' => '{discountValue}% OFF on your entire cart!',
                    'footer' => 'You received this email because you’ve started a checkout at {siteName} and didn’t finish. If you have any questions, please feel free to reach out to us at:

{storeEmail}
{unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                    'cart_title' => 'Your cart:',
                    'product_description' => 'Product',
                    'product_price' => 'Price',
                    'product_qty' => 'Quantity',
                    'product_total' => 'Total',
                    'cart_total' => 'Total:',
                    'button_text' => 'Give Me My {discountValue}% Discount',
                    'headline' => '',
                    'url' => '',
                ]
            ],
        ],
        'sms' => [
            [
                'discount_type' => 0,
                'discount_value' => 0,
                'sending_time' => 1,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '{siteName}: Hi there! We noticed you forgot something in your cart. Complete your order now! {cartLink}',
                    'unsubscribe_text' => 'Unsubscribe',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 15,
                'sending_time' => 24,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '{siteName}: We want to give you {discountValue}% OFF the items your left in your cart! Complete it now: {cartLink}',
                    'unsubscribe_text' => 'Unsubscribe',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 30,
                'sending_time' => 48,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '{siteName}: {discountValue}% OFF on the items you loved. Don\'t miss them, Get it now {cartLink}',
                    'unsubscribe_text' => 'Unsubscribe',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '',
                    'url' => '',
                ]
            ],
        ],
        'push' => [
            [
                'discount_type' => 0,
                'discount_value' => 0,
                'sending_time' => 30,
                'sending_type' => 0,
                'is_default' => 1,
                'body' => [
                    'body_text' => 'Oops... Seems like you didn\'t complete your purchase. CLICK HERE to complete it now >>',
                    'unsubscribe_text' => '',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => 'You order was not processed',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 15,
                'sending_time' => 24,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => 'Get {discountValue}% OFF on the items your left in your cart >>',
                    'unsubscribe_text' => '',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '{firstName}, your cart is still waiting',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 15,
                'sending_time' => 48,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '{firstName}, click to claim your {discountValue}% discount before it\'s too late >>',
                    'unsubscribe_text' => '',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '[Reminder] your {discountValue}% discount is still waiting',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 30,
                'sending_time' => 72,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => '{firstName}, your deal is waiting inside. CLICK HERE >>',
                    'unsubscribe_text' => '',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => 'Complete your purchase with {discountValue}% OFF',
                    'url' => '',
                ]
            ],
            [
                'discount_type' => 0,
                'discount_value' => 30,
                'sending_time' => 72,
                'sending_type' => 1,
                'is_default' => 1,
                'body' => [
                    'body_text' => 'CLICK HERE to complete your purchase now>>',
                    'unsubscribe_text' => '',
                    'subject' => '',
                    'email_preview' => '',
                    'header' => '',
                    'before_cart_body' => '',
                    'after_cart_body' => '',
                    'discount_banner_text' => '',
                    'footer' => '',
                    'cart_title' => '',
                    'product_description' => '',
                    'product_price' => '',
                    'product_qty' => '',
                    'product_total' => '',
                    'cart_total' => ':',
                    'button_text' => '',
                    'headline' => '[LAST CHANCE] Get your items for {discountValue}% OFF',
                    'url' => '',
                ]
            ],
        ],
    ],
    'campaign' => [
        [
            'discount_type' => 0,
            'discount_value' => 0,
            'sending_time' => 30,
            'sending_type' => 0,
            'is_default' => 1,
            'body' => [
                'body_text' => '',
                'unsubscribe_text' => '',
                'subject' => 'Oops... Left Something Behind?',
                'email_preview' => 'Show a little preview of what your email is about!',
                'header' => 'Your Cart is Waiting!',
                'before_cart_body' => 'Hey there!

Thanks for stopping by {siteName}! We noticed that you really liked our products and we have saved them in your cart, just for you!',
                'after_cart_body' => 'The timer is ticking! Check out now before you lose out!
{campaignLink}Click here{/campaignLink} to visit Store.
Thanks for shopping,
The {siteName} Team',
                'discount_banner_text' => 'Your Cart Will Expire Soon!',
                'footer' => 'This email is sent to you using Carti

 {unsubscribeLink}Unsubscribe{/unsubscribeLink}',
                'cart_title' => 'Your cart:',
                'product_description' => 'Product',
                'product_price' => 'Price',
                'product_qty' => 'Quantity',
                'product_total' => 'Total',
                'cart_total' => 'Total:',
                'button_text' => 'Go to Checkout',
                'headline' => '',
                'url' => '',
            ]
        ],
    ]
];
