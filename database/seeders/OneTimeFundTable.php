<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OneTimeFundTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oneTimePlans = [
              ['price'=> 20, 'bonus' => 0],
              ['price'=> 40, 'bonus' => 0],
              ['price'=> 80, 'bonus' => 0],
              ['price'=> 120, 'bonus' => 2],
              ['price'=> 200, 'bonus' => 6],
              ['price'=> 500, 'bonus' => 20],
              ['price'=> 1000, 'bonus' => 50],
              ['price'=> 2000, 'bonus' => 120],
              ['price'=> 4000, 'bonus' => 240],
              ['price'=> 6000, 'bonus' => 360],
              ['price'=> 8000, 'bonus' => 500],
              ['price'=> 10000, 'bonus' => 650],
          ];

        foreach ($oneTimePlans as $key=>$value){
            \DB::table('rc_one_time_funds')->insert([
                'price'=> $value['price'],
                'bonus'=> $value['bonus'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
