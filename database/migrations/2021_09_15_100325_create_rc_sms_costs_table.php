<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcSmsCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_sms_costs', function (Blueprint $table) {
            $table->id();
            $table->string('iso', 5)->nullable();
            $table->string('country', 50)->nullable();
            $table->string('description')->nullable();
            $table->decimal('price', 8, 4)->nullable();
            $table->integer('dial_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_sms_costs');
    }
}
