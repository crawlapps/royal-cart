<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCurrencyToRcAbandonedCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_abandoned_checkouts', function (Blueprint $table) {
            $table->string('currency')->after('checkout_created_at')->nullable();
            $table->string('currency_symbol', 10)->after('currency')->nullable();
            $table->decimal('total_line_items_price', 8,2)->after('currency_symbol')->nullable();
            $table->decimal('total_price', 8,2)->after('total_line_items_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_abandoned_checkouts', function (Blueprint $table) {
            //
        });
    }
}
