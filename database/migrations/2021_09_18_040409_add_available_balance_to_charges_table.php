<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAvailableBalanceToChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('charges', function (Blueprint $table) {
            $table->decimal('balance_used', 8,4)->after('reference_charge')->default(0);
            $table->decimal('balance_remaining', 8,4)->after('balance_used')->default(0);
            $table->decimal('bonus', 8,4)->after('balance_remaining')->default(0);
            $table->decimal('bonus_used', 8,4)->after('bonus')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('charges', function (Blueprint $table) {
            //
        });
    }
}
