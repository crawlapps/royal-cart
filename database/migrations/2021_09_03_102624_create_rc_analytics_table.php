<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_analytics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shop_id')->nullable()->comment('ID from shops table');
            $table->string('type')->nullable()->comment('all, sms, email, push, campaign');
            $table->date('date')->nullable();
            $table->integer('orders')->default(0);
            $table->decimal('revenue', 8, 4)->default(0);
            $table->integer('abandoned_orders')->default(0);
            $table->decimal('spent', 8, 4)->default(0);
            $table->integer('subscriber')->default(0);
            $table->integer('unsubscriber')->default(0);
            $table->integer('sent')->default(0);
            $table->integer('received')->default(0);
            $table->timestamps();

            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_analytics');
    }
}
