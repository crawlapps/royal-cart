<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcAutomationTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_automation_tracks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');

            $table->unsignedBigInteger('db_checkout_id')->nullable();
            $table->unsignedBigInteger('automation_id');

            $table->string('automation_type')->nullable();
            $table->boolean('is_success')->default(0)->comment('is automation is successfully sent or not');
            $table->string('sh_discount_id')->comment('shopify discount coupon id')->nullable();
            $table->string('sh_discount_code')->comment('shopify discount coupon code')->nullable();

            $table->boolean('is_sent')->default(0);
            $table->boolean('is_clicked')->default(0);
            $table->boolean('is_opened')->default(0);
            $table->boolean('is_ordered')->default(0);
            $table->integer('ctr')->nullable()->default(0);
            $table->integer('cvr')->nullable()->default(0);
            $table->integer('roi')->nullable()->default(0);
            $table->decimal('total_revenue', 8,4)->nullable()->default(0);
            $table->decimal('cost',8,4)->nullable()->default(0);

            $table->dateTime('ordered_at')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('db_checkout_id')->on('rc_abandoned_checkouts')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('automation_id')->on('rc_automations')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_automation_tracks');
    }
}
