<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcAutomationBodyTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_automation_body_texts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');
            $table->unsignedBigInteger('automation_id');

//            sms body text
            $table->longText('body_text')->nullable()->comment('store body data for sms and push');
            $table->string('unsubscribe_text', 20)->nullable();

//            email body text
            $table->longText('subject')->nullable();
            $table->longText('email_preview')->nullable();
            $table->longText('header')->nullable();
            $table->longText('before_cart_body')->nullable();
            $table->longText('after_cart_body')->nullable();
            $table->longText('discount_banner_text')->nullable();
            $table->longText('footer')->nullable();

            $table->string('cart_title')->nullable();
            $table->string('product_description')->nullable();
            $table->string('product_price')->nullable();
            $table->string('product_qty')->nullable();
            $table->string('product_total')->nullable();
            $table->string('cart_total')->nullable();
            $table->string('button_text')->nullable();

//                push body text

            $table->string('headline')->nullable();
            $table->string('url')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('automation_id')->on('rc_automations')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_automation_body_texts');
    }
}
