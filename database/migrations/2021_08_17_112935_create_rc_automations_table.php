<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcAutomationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_automations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');

            $table->string('reminder_type')->nullable()->comment('sms, push, email')->default('sms');
            $table->boolean('discount_type')->nullable()->comment('0 => Automatic, 1 => Pre-made')->default(0);
            $table->string('discount_value')->nullable()->comment('How much discount will create on sent reminder')->default(0);
            $table->string('discount_code')->nullable()->comment('Code will add here if user want to add pre made discount(discount_type == 1)')->default(0);
            $table->integer('sending_time')->nullable();
            $table->boolean('sending_type')->nullable()->default(0)->comment('0 = minutes, 1 = hours');
            $table->integer('open_rate')->nullable()->comment('for sms, email reminder type')->default(0);
            $table->integer('total_sent')->nullable()->default(0);
            $table->integer('total_clicked')->nullable()->default(0);
            $table->integer('total_opened')->nullable()->default(0);
            $table->integer('total_order')->nullable()->default(0);
            $table->integer('ctr')->nullable()->default(0);
            $table->integer('cvr')->nullable()->default(0);
            $table->integer('roi')->nullable()->default(0);
            $table->decimal('total_revenue',8,4)->nullable()->default(0);
            $table->decimal('cost',8,4)->nullable()->default(0);
            $table->boolean('is_default')->nullable()->comment('Reminder is created by user or default, 0 = user, 1 = default')->default(0);
            $table->boolean('is_active')->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_automations');
    }
}
