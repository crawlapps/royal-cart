<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAutomationTypeToRcAutomationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_automations', function (Blueprint $table) {
            $table->string('automation_type')->after('reminder_type')->nullable()->comment('automation or campaign')->default('automation');
            $table->boolean('campaign_sending_type')->after('sending_type')->default(0)->comment('0 = schedule now, 1 = schedule later');
            $table->dateTime('campaign_sending_time')->after('campaign_sending_type')->nullable()->comment('campaign sending time if it schedule later');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_automations', function (Blueprint $table) {
            //
        });
    }
}
