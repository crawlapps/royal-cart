<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcUsageChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_usage_charges', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->comment('ID from users table');
            $table->unsignedBigInteger('shop_id')->nullable()->comment('ID from shops table');
            $table->unsignedBigInteger('db_charge_id')->nullable()->comment('ID from charges table');
            $table->unsignedBigInteger('db_automation_track_id')->nullable()->comment('ID from automation track table');
            $table->string('shopify_charge_id')->nullable()->comment('Shopify charge id');
            $table->string('shopify_usagecharge_id')->nullable()->comment('Shopify usage charge id');
            $table->integer('plan_id')->nullable()->comment('ID from plans table');
            $table->string('description')->nullable();
            $table->decimal('price',8,4)->nullable();
            $table->decimal('balance_used',8,4)->nullable();
            $table->decimal('balance_remaining',8,4)->nullable();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('db_automation_track_id')->on('rc_automation_tracks')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_usage_charges');
    }
}
