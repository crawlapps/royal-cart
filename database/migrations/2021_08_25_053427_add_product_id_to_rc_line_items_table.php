<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProductIdToRcLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_line_items', function (Blueprint $table) {
            $table->string('sh_product_id')->after('db_checkout_id')->nullable();
            $table->string('sh_variant_id')->after('sh_product_id')->nullable();
            $table->string('product_handle')->after('sh_variant_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_line_items', function (Blueprint $table) {
            //
        });
    }
}
