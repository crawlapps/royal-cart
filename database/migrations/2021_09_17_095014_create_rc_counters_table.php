<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_counters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable()->comment('ID from users table');
            $table->unsignedBigInteger('db_charge_id')->nullable()->comment('ID from charges table');
            $table->string('shopify_charge_id')->nullable()->comment('Shopify charge id');
            $table->integer('plan_id')->nullable()->comment('ID from plans table');
            $table->decimal('fund', 8, 2)->nullable()->comment('recurring fund');
            $table->string('fund_type')->nullable()->comment('recurring or one time');
            $table->integer('recur_min')->nullable()->comment('When my balance falls below given amount');
            $table->integer('recur_max')->nullable()->comment('Never exceed per billing cycle');
            $table->integer('onetime_amount')->nullable();
            $table->integer('onetime_bonus')->nullable();
            $table->decimal('onetime_bonus_used', 8,4)->nullable();
            $table->string('status')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_counters');
    }
}
