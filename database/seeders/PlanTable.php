<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PlanTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Free"),
                'price' => env('PLAN_PRICE_1', 0.00),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 1000.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)0-100"),
                'trial_days' => env('TRIAL_DAY', 30),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 100
            ],
            [
                'id' => 2,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$4.99/month"),
                'price' => env('PLAN_PRICE_1', 4.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)100-200"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 200
            ],
            [
                'id' => 3,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$9.99/month"),
                'price' => env('PLAN_PRICE_1', 9.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)200-500"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 500
            ],
            [
                'id' => 4,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$19.99/month"),
                'price' => env('PLAN_PRICE_1', 19.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)500-1k"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 1000
            ],
            [
                'id' => 5,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$39.99/month"),
                'price' => env('PLAN_PRICE_1', 39.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)1K-2K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 2000
            ],
            [
                'id' => 6,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$89.99/month"),
                'price' => env('PLAN_PRICE_1', 89.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)2K-5K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 5000
            ],
            [
                'id' => 7,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$169.99/month"),
                'price' => env('PLAN_PRICE_1', 169.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)5K-10K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 10000
            ],
            [
                'id' => 8,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$249.99/month"),
                'price' => env('PLAN_PRICE_1', 249.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)10K-20K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 20000
            ],
            [
                'id' => 9,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$499.99/month"),
                'price' => env('PLAN_PRICE_1', 499.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)20K-50K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 50000
            ],
            [
                'id' => 10,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$999.99/month"),
                'price' => env('PLAN_PRICE_1', 999.99),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)50K-100K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 100000
            ],
            [
                'id' => 11,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "$1.5k/month"),
                'price' => env('PLAN_PRICE_1', 1500),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Extra sales generated ($)+100K"),
                'trial_days' => env('TRIAL_DAY', 0),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1),
                'upto' => 'unlimited'
            ],
        ]);
    }
}
