<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRcWelcomePushesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rc_welcome_pushes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('shop_id');

            $table->boolean('discount_type')->nullable()->comment('0 => Automatic, 1 => Pre-made')->default(0);
            $table->string('discount_value')->nullable()->comment('How much discount will create on sent reminder')->default(0);
            $table->string('discount_code')->nullable()->comment('Code will add here if user want to add pre made discount(discount_type == 1)')->nullable();
            $table->string('logo')->nullable();
            $table->string('headline')->nullable();
            $table->longText('body_text')->nullable();
            $table->string('url')->nullable();
            $table->boolean('active')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->on('users')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
            $table->foreign('shop_id')->on('rc_shops')->references('id')->onUpdate('NO ACTION')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rc_welcome_pushes');
    }
}
