<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddToFromColumnToRcAutomationTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_automation_tracks', function (Blueprint $table) {
            $table->string('to')->after('cost')->nullable();
            $table->string('from')->after('to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_automation_tracks', function (Blueprint $table) {
            //
        });
    }
}
