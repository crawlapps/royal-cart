<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClickOpenColumnsToRcAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rc_analytics', function (Blueprint $table) {
            $table->integer('clicks')->after('received')->default(0);
            $table->integer('opened')->after('clicks')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rc_analytics', function (Blueprint $table) {
            //
        });
    }
}
